#!/bin/sh

#THIS SCRIPT IS A BEGINNING, IT'S ABSOLUETLY NOT FUNCTIONAL AND DONE YET, I'M CURRENTLY WORKING ON IT

if [[`whoami` != 'root']]; then
	echo 'This script is supposed to be run by the root account, not with sudo privileges, but by root'
	exit 1
fi


# UPGRADE PORT TREES AND INSTALL PACKAGE UPDATES
echo '----------------------------------------------------------------------------------------------------------'
echo '----------------------------------------------------------------------------------------------------------'
echo 'upgrade port trees'
echo '----------------------------------------------------------------------------------------------------------'
echo '----------------------------------------------------------------------------------------------------------'
sleep 1
portsnap fetch
pkg update && pkg upgrade -y

echo '----------------------------------------------------------------------------------------------------------'
echo '----------------------------------------------------------------------------------------------------------'
echo 'Install all the packages I need'
echo '----------------------------------------------------------------------------------------------------------'
echo '----------------------------------------------------------------------------------------------------------'
sleep 1
pkg install -y vim htop zsh curl git wget sudo terminator

sleep 1

# ADD THE USER TO WHEEL GROUP, AUTHORIZE WHEEL GROUP TO USE SUDO AND DEFINE ZSH AS MAIN SHELL AND DOWNLOAD OH MY ZSH
echo "We're going to change few parameters of your main user"
echo 'what is the username of your main user ? :'
read username

#THE PROPER WAY IS WITH VISUDO, DIDN'T FIND A WAY TO PROPERLY INCORPORATE IT IN THE SCRIPT
echo '%wheel ALL=(ALL:ALL) ALL' >> /usr/local/etc/sudoers
usermod -G wheel $username


#BECOMING THE MAIN USER, WE'LL BE ROOT AGAIN SOON
su $username

if [[ `echo $SHELL` != 'zsh$' ]]; then
  echo '----------------------------------------------------------------------------------------------------------'
  echo '----------------------------------------------------------------------------------------------------------'
	echo "install and set zsh as main shell for your user"
  echo '----------------------------------------------------------------------------------------------------------'
  echo '----------------------------------------------------------------------------------------------------------'

	chsh -s /usr/local/bin/zsh
  echo '----------------------------------------------------------------------------------------------------------'
  echo '----------------------------------------------------------------------------------------------------------'
	echo 'download and install Oh My Zsh'
  echo '----------------------------------------------------------------------------------------------------------'
  echo '----------------------------------------------------------------------------------------------------------'
	sleep 1
	sh -c "$(wget https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh -O -)"
fi

echo '----------------------------------------------------------------------------------------------------------'
echo '----------------------------------------------------------------------------------------------------------'
echo 'download DWM suckless, simple terminal, dmenu and surf'
echo '----------------------------------------------------------------------------------------------------------'
echo '----------------------------------------------------------------------------------------------------------'
sleep 1

# CREATE THE FOLDER WHERE I'LL DOWNLOAD THOSE GIT REPOS
for i in dwm dmenu st surf; do
	mkdir -p ~/git/$i;
	git clone https://git.suckless.org/$i $i/;
done
